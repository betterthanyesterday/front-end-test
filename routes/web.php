<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Website\Home@index')->name('home');
Route::get('/services', 'Website\Services@index')->name('services');
Route::get('/case-studies', 'Website\CaseStudy@index')->name('case-studies');
Route::get('/blog', 'Website\Blog@index')->name('blog');
Route::get('/about-us', 'Website\About@index')->name('about');
Route::get('/contact-us', 'Website\Contact@index')->name('contact');
