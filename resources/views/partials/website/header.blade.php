<div class="header-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="logo">
                    <a href="{{ route('home') }}">
                        <img src="{{ asset('images/logo.png')  }}" alt="MindYerAppLogo">
                    </a>
                </div>
            </div>

            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                <div class="nav-wrapper desktop">
                    <ul class="nav">
                        <li><a class="@yield('about-active')" href="{{ route('about') }}">ABOUT</a></li>
                        <li><a class="@yield('services-active')" href="{{ route('services') }}">SERVICES</a></li>
                        <li><a class="@yield('case-study-active')" href="{{ route('case-studies') }}">CASE STUDIES</a></li>
                        <li><a class="@yield('blog-active')" href="{{ route('blog') }}">BLOG</a></li>
                        <li><a class="@yield('contact-active')" href="{{ route('contact') }}">CONTACT US</a></li>
                    </ul>
                </div>

                <div class="nav-wrapper mobile">
                    <div class="content">
                        <nav>
                            <input type="checkbox" id="hamburger1" />
                            <label for="hamburger1"></label>

                            <ul class="nav-links">
                                <li><a class="@yield('about-active')" href="{{ route('about') }}">ABOUT</a></li>
                                <li><a class="@yield('services-active')" href="{{ route('services') }}">SERVICES</a></li>
                                <li><a class="@yield('case-study-active')" href="{{ route('case-studies') }}">CASE STUDIES</a></li>
                                <li><a class="@yield('blog-active')" href="{{ route('blog') }}">BLOG</a></li>
                                <li><a class="@yield('contact-active')" href="{{ route('contact') }}">CONTACT US</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
