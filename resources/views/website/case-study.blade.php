@extends('layouts.website')

@section('page-title', 'Case Studies')

@section('page-styles')
@endsection

@section('case-study-active', 'active')

@section('content')
    <div class="container">
        <section class="pov">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="left-pov fade-in">
                        <img class="bg" src="{{ asset('images/faded-symbol.png') }}" alt="">
                        <h1>Case Studies</h1>
                        <p>Sample Content</p>
                        <button class="btn btn-primary btn-override">
                            <img src="{{ asset('images/arrows-right.png') }}" alt=""> Sample Button
                        </button>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="right-pov">
                        <img class="fade-in" src="{{ asset('images/illustration.png')  }}" alt="illustration">
                    </div>
                </div>
            </div>
        </section>
    </div>

    <section class="who-we-are">

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="content">
                        <img src="{{ asset('images/faded-symbol-darker.png')  }}" alt="">
                        <h3>WHO WE ARE</h3>
                        <h1>We love to solve problems</h1>
                        <p>Think there is a better way to perform mundane or time-consuming tasks? Quite often there is and we're here to solve your conundrum with technology! Put our innovative thinking to the test and share your brief with us today</p>
                        <button class="btn btn-warning btn-warning-override">
                            DISCUSS A PROJECT
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('page-scripts')
@endsection

